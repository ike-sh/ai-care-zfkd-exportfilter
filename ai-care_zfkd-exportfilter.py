# import required modules
from datetime import timedelta, datetime
import gc
from glob import glob
from os import linesep, mkdir, path, remove
from shutil import move as move_file
import xml.etree.ElementTree as ET

# register namespace and set up a shorthand identifier
ET.register_namespace("", "http://www.basisdatensatz.de/oBDS/XML")
ns = {"obds": "http://www.basisdatensatz.de/oBDS/XML"}


## Utility functions ##

def difference_in_percent(previous, current) -> float:
    """Calculates the percentage difference between two numbers."""

    if current == previous:
        return 0
    try:
        percentage = (current - previous) / max(previous, current) * 100
    except ZeroDivisionError:
        percentage = float("inf")
    return percentage


def print_percentage(percentage: float) -> str:
    """Formats a percentage with a comma as decimal separator."""

    return f"{percentage:.2f}%".replace(".", ",")


def print_int(integer: int) -> str:
    """Formats an integer with a dot as decimal separator."""
    return f"{integer:,}".replace(",", ".")


def print_summary(
    filename: str, initial_patient_count: int, remaining_patient_count: int
) -> None:
    """Prints a summary of initial and remaining patient counts.
    
    Args:
        filename: The name of the processed file.
        initial_patient_count: The initial patient count.
        remaining_patient_count: The remaining patient count after filtering.
    """

    delta = print_percentage(
        difference_in_percent(initial_patient_count, remaining_patient_count)
    )

    print(
        f"summary for {filename}:" + linesep,
        "  initial patient count: " + print_int(initial_patient_count) + linesep,
        "  remaining patients after filtering: "
        + print_int(remaining_patient_count)
        + f" ({delta})",
        linesep,
    )


## Date processing functions ##

date_accuracy_offset_in_years = {
   """Offsets for date comparison for common accuracy markers.

   Note: `V` is explicitly not defined as it marks a date as completely estimated and therefore not comparable
   in any reasonable way. 
      
   E: The date is accurate to the day, no offset required when calculating the difference between two dates.

   T: The date is accurate to the month (-> the day is always the 15th of a month).
      An offset of a month (365/30 ≈ 0.083 years) is required to account for edge cases where the earlier date
      is actually after the 15th of the month and the other date is actually before the 15th of the month.

   M: The date is accurate to the year. An offset of a year is required in case the earlier date is actually
      at the end of the year and the other date is actually at the start of the year.
   """

    "E" : 0,
    "T" : 31/365, # 1 month (≈ 0.085 years)
    "M" : 1,
}

def get_date_accuracy_marker_offset(date_element: ET.Element) -> int | float:
    """Parses the accuracy marker of a date element and returns the offset if successful.
    
    Args:
        date_element (ET.Element): an instance of `obds:Geburtsdatum` or `obds:Diagnosedatum`

    Raises:
        Exception: if the accuracy marker is not present on the date element or does not allow reasonable date comparison

    Returns:
        int | float: an offset for date comparison if a valid accuracy marker could be determined
    """
    
    accuracy_marker = date_element.get('Datumsgenauigkeit')

    if accuracy_marker == None:
        raise Exception("Accuracy marker not defined on date element!")

    if not accuracy_marker in date_accuracy_offset_in_years:
        raise Exception(f"Accuracy marker `{accuracy_marker}` does not allow reasonable date comparison!")
    
    return date_accuracy_offset_in_years[accuracy_marker]


def parse_date_element(date_element: ET.Element) -> tuple[datetime, int | float]:
    """Parses the date element along with its accuracy marker.
    
    Args:
        date_element (ET.Element): a date element, e.g. `obds:Geburtsdatum` or `obds:Diagnosedatum`

    Raises:
        Exception: if the date element is invalid or can't be parsed properly

    Returns:
        parsed_date_and_offset: a tuple of the parsed `datetime` and an `int` or `float`
    """

    if (date_element == None):
        raise Exception("Date element type is `None`")
    
    parsed = datetime.fromisoformat(date_element.text)

    accuracy_marker = get_date_accuracy_marker_offset(date_element)

    return (parsed, accuracy_marker)


def date_difference_in_years(date1: datetime, date2: datetime) -> int | float:
    """Calculates the absolute difference between two dates in years.
    
    Args:
        date1 (datetime): the first date
        date2 (datetime): the second date
    """

    return abs((date1 - date2) / timedelta(days=365.2425))


## XML tree filter functions ##

def tumor_meets_min_age_critera(
    patient_birthdate_and_offset: tuple[datetime, int | float],
    tumor: ET.Element,
    min_age_at_time_of_diagnosis_in_years: int | None = None,
    debug: bool = False,
) -> bool:
    """Validates whether the age of the patient at the time of tumor diagnosis meets the minimum age.

    Args:
        patient_birthdate_and_offset (tuple[datetime, int | float] | None): the parsed birthdate of the patient and the corresponding accuracy offset
        tumor (ET.Element): an instance of `obds:Tumor`
        min_age_at_time_of_diagnosis_in_years: the minimum age the patient must have reached at the time of the tumor diagnosis
        debug (bool): whether to print additional information during execution for debugging purposes

    Raises:
        Exception: if the date comparison can't be performed due to invalid or missing dates or accuracy offsets

    Returns:
        was_old_enough: a boolean that indicates whether the patient was old enough at the time of the tumor diagnosis
    """

    if min_age_at_time_of_diagnosis_in_years == None:
        # no min age at time of diagnosis defined, the criteria is always met
        return True

    (patient_birthdate, patient_birthdate_offset) = patient_birthdate_and_offset

    (tumor_diagnosis_date, tumor_diagnosis_date_offset) = parse_date_element(
        tumor.find("obds:Primaerdiagnose/obds:Diagnosedatum", ns)
    )

    # in case the accuracy offsets are not equal, we use the greater one
    # TODO: this probably could be optimized a little bit
    accuracy_offset = max(patient_birthdate_offset, tumor_diagnosis_date_offset)

    age_at_diagnosis_in_years = date_difference_in_years(
        patient_birthdate, tumor_diagnosis_date
    )

    min_age_with_offset = min_age_at_time_of_diagnosis_in_years + accuracy_offset
    was_old_enough = age_at_diagnosis_in_years >= min_age_with_offset

    if debug and not was_old_enough:
        print(
            f'min age criteria not met; birthdate: {patient_birthdate.strftime("%Y-%m-%d")}, diagnosis date: {tumor_diagnosis_date.strftime("%Y-%m-%d")}, age at diagnosis: {age_at_diagnosis_in_years}, min age (with offset of {accuracy_offset}): {min_age_with_offset}'
        )

    return was_old_enough


def tumor_matches_valid_icds(
    tumor: ET.Element,
    valid_icds: tuple[str, ...],
) -> bool:
    """Checks whether the tumor matches one of the valid ICD codes.

    Args:
        tumor (ET.Element): an instance of `obds:Tumor`
        valid_icds (tupe[str, ...]): a tuple of ICD codes, of which one should match the ICD code of the tumor

    Raises:
        Exception: if the tumor's ICD code could not be found

    Returns:
        is_matching_icd: a boolean indicating whether the tumor matches one of the valid ICD codes
    """

    tumor_icd = tumor.find("obds:Primaerdiagnose/obds:Primaertumor_ICD/obds:Code", ns)

    if tumor_icd == None:
        raise Exception("The ICD code of the tumor could not be found!")

    return tumor_icd.text.startswith(valid_icds)


def apply_tumor_filter_on_patient(
    patient: ET.Element,
    valid_icds: tuple[str, ...],
    keep_mismatches: bool = False,
    min_age_at_time_of_diagnosis_in_years: int | float | None = None,
    debug: bool = False,
) -> bool:
    """Checks the tumors of a single patient and removes tumors that don't match the list of valid ICD codes (with optional opt-out).

    Note: If `min_age_at_time_of_diagnosis_in_years` is defined, all tumors that don't fulfill this criteria are
    removed from the tree without any ICD code validation.

    Args:
        patient (ET.Element): an instance of `obds:Patient`
        valid_icds (tuple[str, ...]): a tuple of ICD codes that defines which tumors should not be removed by any means
        keep_mismatches (bool): whether to keep non-matching tumors or not
        min_age_at_time_of_diagnosis_in_years (int | float | None): the minimum age at the time of the tumor diagnosis in years (optional, not checked if `None`)
        debug (bool): whether to print additional information during execution for debugging purposes

    Returns:
        has_matching_tumor: a boolean indicating whether the patient has any tumor that matches the list of valid ICD codes (and fulfills the min age criteria, if applicable)
    """

    patient_birthdate_and_offset: tuple[datetime, int | float] | None = None

    # parse the patient's birthdate if a min age criteria is defined
    if min_age_at_time_of_diagnosis_in_years != None:
        try:
            patient_birthdate_and_offset = parse_date_element(
                patient.find("obds:Patienten_Stammdaten/obds:Geburtsdatum", ns)
            )
        except Exception as e:
            print(e)
            # the patient's age could not be reliably determined, so we have to assume
            # that none of the patient's tumors meet the min age criteria and that the
            # patient should therefore be completely removed from the output
            return False

    # get a reference to the tumor list element
    tumor_list = patient.find("obds:Menge_Tumor", ns)

    # get all tumors of the patient
    tumors = patient.findall("obds:Menge_Tumor/obds:Tumor", ns)

    # a flag to keep track of tumor matches
    has_tumor_matching_criteria = False

    for tumor in tumors:
        try:
            if (
                min_age_at_time_of_diagnosis_in_years != None
                and not tumor_meets_min_age_critera(
                    patient_birthdate_and_offset,
                    tumor,
                    min_age_at_time_of_diagnosis_in_years,
                    debug,
                )
            ):
                # the patient was not old enough at the time of the tumor diagnosis,
                # remove the tumor from the tree
                tumor_list.remove(tumor)

            elif not tumor_matches_valid_icds(tumor, valid_icds):
                # the tumor does not match any valid ICD code, remove it from
                # the tree if requested or if the patient was not old enough
                # at the time of its diagnosis
                if not keep_mismatches:
                    tumor_list.remove(tumor)

            elif not has_tumor_matching_criteria:
                # a matching tumor was found and the flag
                has_tumor_matching_criteria = True

                if keep_mismatches:
                    # shortcut in case mismatched tumors should not be removed and the patient
                    # age at the time of the tumor diagnosis does not matter:
                    # if we reach this condition, it means that at least one tumor was found
                    # and remaining iterations can be skipped for improved performance
                    break

        except Exception as e:
            # something went wrong (e.g. date comparison), remove to ensure that possibly invalid patient data is not included
            tumor_list.remove(tumor)
            print(e)

    return has_tumor_matching_criteria


def apply_tumor_filter_on_tree(
    tree_root: ET.Element,
    valid_icds: tuple[str, ...],
    min_age_at_time_of_diagnosis_in_years: int | None = None,
    keep_mismatches: bool = False,
    debug: bool = False,
) -> None:
    """Finds patients and applies the tumor filter on each patient instance.

    Note: If all tumors of a patient are removed or no matching tumors were found,
    the patient instance is removed altogether.

    Args:
        tree_root (ET.Element): the root element of the XML tree
        valid_icds (tupe[str,...]): a tuple of ICD codes that defines which tumors should not be removed by any means
        min_age_at_time_of_diagnosis_in_years (int | None): the minimum age at the time of the tumor diagnosis in years (optional, not checked if `None`)
        keep_mismatches (bool): whether to keep non-matching tumors or not
        debug (bool): whether to print additional information during execution for debugging purposes
    """

    # get a reference to the patient list element
    patient_list = tree_root.find("obds:Menge_Patient", ns)

    for patient in patient_list.findall("obds:Patient", ns):
        has_tumor_matching_icd = apply_tumor_filter_on_patient(
            patient,
            valid_icds,
            keep_mismatches,
            min_age_at_time_of_diagnosis_in_years,
            debug,
        )

        if not has_tumor_matching_icd:
            # the patient does not have any tumor that matches the valid ICD codes,
            # remove the patient instance from the tree altogether
            patient_list.remove(patient)


## Batch processing functions ##

def process_file(
    in_dir: str,
    filename: str,
    valid_icds: tuple[str, ...],
    min_age_at_time_of_diagnosis_in_years: int | None = None,
    keep_mismatches: bool = False,
    out_dir: str = "./out/",
    out_suffix: str = "_filtered",
    debug: bool = False,
) -> tuple[int, int]:
    """Processes a single XML file and stores the filtered output in the `out_dir`.

    Args:
        in_dir (str): the directory where the input XML file is located
        filename (str): the name of the input XML file
        valid_icds (tuple[str, ...]): a tuple of ICD codes that defines which tumors should be kept in the output XML file
        min_age_at_time_of_diagnosis_in_years (int | None): the minimum age at the time of the tumor diagnosis in years (optional, not checked if `None`)
        keep_mismatches (bool): whether all tumors of a patient that don't match the valid ICD codes should be kept in the output XML file if the patient has a matching tumor
        out_dir (str): the directory where the output XML file will be stored
        out_suffix (str): the suffix to append to the output file name
        debug (bool): whether to print additional information during execution for debugging purposes

    Returns:
        counts (tuple[int, int]): A tuple of the initial patient count and the patient count after filtering.
    """

    # the output file name is based in the input file name
    output_name = filename.replace(".xml", out_suffix + ".xml")

    # load and parse the file
    tree = ET.parse(in_dir + filename)
    root = tree.getroot()

    # get an initial count of patients in the XML file (used in the summary at the end of the script)
    initial_patient_count = len(root.findall("obds:Menge_Patient/obds:Patient", ns))

    # apply filter
    apply_tumor_filter_on_tree(
        root, valid_icds, min_age_at_time_of_diagnosis_in_years, keep_mismatches, debug
    )

    # write the modified XML tree to the output file
    tree.write(out_dir + output_name)

    # get the remaining patient count
    remaining_patient_count = len(root.findall("obds:Menge_Patient/obds:Patient", ns))

    # delete tree object to release memory
    del tree
    gc.collect()

    # return initial and remaining patient counts for evaluation purposes
    return (initial_patient_count, remaining_patient_count)


def get_patients_from_xml_file(in_dir: str, filename: str) -> list[ET.Element]:
    """Extracts all patient elements from the input XML file.

    Args:
        in_dir (str): the directory where the input XML file is located
        filename (str): the name of the input XML file

    Returns:
        patients: A list of `obds:Patient` elements.
    """

    # load and parse the file
    tree = ET.parse(in_dir + filename)
    root = tree.getroot()

    # return all patient elements
    return root.findall("obds:Menge_Patient/obds:Patient", ns)


def merge_xml_files(in_dir: str, out_dir: str, out_name: str) -> int | None:
    """Merges all XML files in `in_dir` into a single XML file and stores it in `out_dir`.

    Args:
        in_dir (str): the directory where the input XML files are located
        out_dir (str): the directory where the merged output XML file will be stored
        out_name (str): the name of the output XML file

    Raises:
        Exception: if no XML files are found in `in_dir`

    Returns:
        int: the number of patients in the merged XML file
        None: if only one XML file is found in `in_dir` and the files is only renamed and moved to `out_dir`
    """

    # get a list of all XML files in `in_dir`
    files = glob("*.xml", root_dir=in_dir)

    if len(files) < 1:
        raise Exception(f"No XML files found in {in_dir}")

    if len(files) < 2:
        print(
            f"found only one XML file, renaming file to {out_name} and moving it to {out_dir}"
        )
        move_file(in_dir + files[0], out_dir + out_name)
        return

    # sort files to ensure that they are merged in their original order
    files.sort()

    print(
        "merging the following files:" + linesep,
        *map(lambda x: "  " + x + linesep, files),
    )

    # load the first XML file
    base_tree = ET.parse(in_dir + files[0])
    base_tree_root = base_tree.getroot()

    # get a reference to the patient list element
    patient_list = base_tree_root.find("obds:Menge_Patient", ns)

    for file in files[1:]:
        # get the patient elements from the file
        patients = get_patients_from_xml_file(in_dir, file)

        # add the patient elements to the merged tree
        patient_list.extend(patients)

    # store the merged XML tree
    base_tree.write(out_dir + out_name)

    print(f"merged file stored as {out_dir + out_name}" + linesep)

    return len(base_tree_root.findall("obds:Menge_Patient/obds:Patient", ns))


def clean_out_dir(out_dir: str, skip_files: list[str] = []) -> None:
    """Deletes all files from `out_dir` except for files in `skip_files`.

    Args:
        out_dir (str): the path of the directory to clean
        skip_files (list[str]): a list of files to skip when cleaning the directory
    """

    files = glob("*.xml", root_dir=out_dir)

    for file in filter(lambda f: f not in skip_files, files):
        remove(out_dir + file)

    print(f"cleaning output directory {out_dir} ..." + linesep)


def process_directory(
    valid_icds: tuple[str, ...],
    min_age_at_time_of_diagnosis_in_years: int | None = None,
    in_dir: str = "./",
    out_merged_name: str = "out.xml",
    pre_clean_out_dir: bool = True,
    post_clean_out_dir: bool = True,
    keep_mismatches: bool = False,
    out_dir: str = "./out/",
    out_suffix: str = "_filtered",
    debug: bool = False,
) -> None:
    """Processes each XML file in `in_dir` and stores a merged XML file in `out_dir`.

    By default, the output directory is emptied before processing and individual XML
    files are removed from the output directory once the merged XML file is created.

    Args:
        valid_icds (tuple[str,...]): a tuple of ICD codes that defines which tumors should be kept in the output XML file
        min_age_at_time_of_diagnosis_in_years (int | None): the minimum age at the time of the tumor diagnosis in years (optional, not checked if `None`)
        in_dir (str): the directory where the input XML files are located
        out_merged_name (str): the name of the merged XML file stored in the `out_dir`
        pre_clean_out_dir (bool): whether to empty the `out_dir` before processing
        post_clean_out_dir (bool): whether to empty the `out_dir` after processing
        keep_mismatches (bool): whether all tumors of a patient that don't match the valid ICD codes should be kept in the output XML files if the patient has a matching tumor
        out_dir (str): the directory where the output XML files will be stored
        out_suffix (str): the suffix to append to the output file names
        debug (bool): whether to print additional information during execution for debugging purposes

    Raises:
        Exception: if `in_dir` and `out_dir` are the same
        Exception: if the remaining patients count in the merged XML file does not match the sum of the remaining patients in the individual XML files
    """

    if in_dir == out_dir:
        raise Exception(f"`in_dir` and `out_dir` cannot be the same!")

    if path.exists(out_dir) and pre_clean_out_dir:
        clean_out_dir(out_dir)

    if not path.exists(out_dir):
        mkdir(out_dir)

    # get a list of all XML files in `in_dir`
    files = glob("*.xml", root_dir=in_dir)

    # counters to keep track of initial and remaining patient entries
    initial_patient_count = 0
    remaining_patient_count = 0

    # process each file individually
    for file in sorted(files):
        print(f"processing file {file} ...")
        (initial, remaining) = process_file(
            in_dir,
            file,
            valid_icds,
            min_age_at_time_of_diagnosis_in_years,
            keep_mismatches,
            out_dir,
            out_suffix,
            debug,
        )

        # update stats
        initial_patient_count += initial
        remaining_patient_count += remaining

        print_summary(file, initial, remaining)

    # merge processed files into single file
    merged_remaining = merge_xml_files(out_dir, out_dir, out_merged_name)

    if merged_remaining != None:
        # validate whether the counts add up
        if merged_remaining != remaining_patient_count:
            raise Exception(
                f"individual remaining patients {remaining_patient_count:.} != merged remaining patients {merged_remaining:.}"
            )

        print_summary(out_merged_name, initial_patient_count, merged_remaining)

    if post_clean_out_dir:
        clean_out_dir(out_dir, [out_merged_name])
        

## Script configuration and execution ##

# the path of the input directory; update as needed!
in_dir = "./input-directory/"

# the name of the merged output file
out_merged_name = "ZfKD-2014-2022_AI-Care.xml"

# define ICD codes of tumors that should be included in the filtered output
valid_icds = ("C34", "C50", "C73", "C82", "C83", "C84", "C85", "C86", "C87", "C88")

# define how old the patient must have been at the time of a tumor diagnosis
# so that the corresponding tumor is not filtered out
# Note: Accuracy markers on date fields will be automatically respected!
min_age_at_time_of_diagnosis_in_years = 18

# set to `False` if the output directory should not be emptied before running the script
pre_clean_out_dir = True

# set to `False` if the individual out files should not be deleted after
# they have been merged into a single XML file
post_clean_out_dir = True

# set to `False` if tumor that do not match the valid ICD codes should be included in the
# output as long as the corresponding patient has at least one tumor with a matching ICD code
keep_tumor_mismatches = False

# print additional information during execution for debugging purposes
debug = True

process_directory(
    valid_icds,
    min_age_at_time_of_diagnosis_in_years,
    in_dir,
    out_merged_name,
    pre_clean_out_dir,
    post_clean_out_dir,
    keep_tumor_mismatches,
    debug = debug,
)