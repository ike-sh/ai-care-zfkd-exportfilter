# AI-CARE ZfKD-Exportfilter

This script filters a set of XML files that match the ZfKD oBDS XML schema according to a list of ICD codes and a minimum age at the time of the tumor diagnosis. The filtered output is saved in a single XML file, which also conforms to the oBDS schema of the ZfKD.

The script can be either run as a [Jupyter notebook](https://docs.jupyter.org/en/latest/#what-is-a-notebook) or as a regular Python script.

### Filter process

Each XML file from the specified input directory is processed on its own. The filtered output is stored in the `out` directory and subsequently the individual filtered files are merged into a single XML file.

The following filters are applied to the list of patients and their tumors:
1. If a patient's birthdate can't be (reliably) determined, then the patient is completely omitted in the filtered output.
2. If a patient was not at least 18 years old at the time of a tumor diagnosis, then the corresponding tumor is removed from the patient's tumor list.
   Note: The date accuracy markers of the date elements (birthdate and tumor diagnosis date) are respected by adding an offset of 1 month (T) or 1 year (M).
3. Any tumor that does not match the list of valid ICD codes is removed from the patient's tumor list.
4. If a patient does not have any tumor that matches the list of valid ICD codes, then the patient is completely omitted in the filtered output.

## Usage

### Configuration

The following configuration parameters can be customized for both script variants.

The input directory (the directory which contains the exported XML files) must be assigned to the variable `in_dir` of the script.

Optionally, the following variables may be used for further customization:

- `out_merged_name`: The name of the output file which contains the filtered data and is stored in the `out` directory.

- `valid_icds`: A list of ICD codes. All tumors that don't match any of these ICD codes will be filtered out.

- `min_age_at_time_of_diagnosis_in_years`: All tumors which were diagnosed before the corresponding patient was as old as defined by this variable will be filtered out.

- `pre_clean_out_dir`: Whether to delete all files from the `out` directory before script execution.

- `post_clean_out_dir`: Whether to delete all temporarily created files from the `out` directory after script exception (the merged file stored as `out_merged_name` will not be ddeleted from the directory).

- `keep_tumor_mismatches`: Whether to keep tumors that don't match the list of valid ICD codes as long as the corresponding patient has any other tumor matching the list of valid ICD codes.

- `debug`: Whether to print additional information during script execution for debugging purposes to the terminal 

### Jupyter notebook (`.ipynb`)

After configuration in the last notebook cell ("Script configuration and execution"), the notebook must be executed in full from top to bottom.

### Execution of the regular Python script (`.py`)

After configuration ([lines 585-613](https://gitlab.com/ike-sh/ai-care-zfkd-exportfilter/-/blob/main/ai-care_zfkd-exportfilter.py?ref_type=heads#L585-613)), the regular Python script can be executed from a terminal:

```sh
python3 ai-care_zfkd-exportfilter.py
```

## License

[BSD-3-Clause](https://opensource.org/license/bsd-3-clause/)

Copyright (c) 2024, Institute for Cancer Epidemiology, University of Lübeck
